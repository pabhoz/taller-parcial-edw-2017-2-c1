export class Example{

    constructor(a,b){
        this._a = a;
        this._b = b;
        this._DOMElement = null;
        this.initDOMElement();
    }

    get a(){
        return this._a;
    }

    set a(val){
        this._a = val;
    }

    get b(){
        return this._b;
    }

    set b(val){
        this._b = val;
    }

    get DOMElement(){
        return this._DOMElement;
    }

    initDOMElement(){
        let el = document.createElement("div");
        this._DOMElement = el;
    }

}
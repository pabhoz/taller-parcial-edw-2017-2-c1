var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    app: ['./src/app.js']
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'main.bundle.js'
  },
  resolve: {
    extensions: ['.js']
  },
  devServer:{
    host: '0.0.0.0',
    port: 8080,
    inline: true
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules',
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  stats: { colors: true}
}